//go:build mage

package main

import (
	"github.com/magefile/mage/sh"
)

func Clean() error {
	if err := sh.Run("go", "clean"); err != nil {
		return err
	}
	if err := rm(
		"gcoderamp", "gcoderamp.exe",
		"pcb2gcodewrap", "pcb2gcodewrap.exe",
		"spoilboard", "spoilboard.exe",
	); err != nil {
		return err
	}
	return nil
}

func rm(path ...string) error {
	for _, p := range path {
		if err := sh.Rm(p); err != nil {
			return err
		}
	}
	return nil
}
