//go:build mage

package main

import (
	"context"
	"os"
	"runtime"

	"github.com/magefile/mage/mg"
	"github.com/magefile/mage/sh"
	"golang.org/x/sync/errgroup"
)

func Build() {
	mg.Deps(Gcoderamp, Pcb2gcodewrap, Spoilboard, GCodeSplit)
}

func Gcoderamp() error {
	err := sh.Run("go", "build", "./cmd/gcoderamp")
	if err != nil {
		return err
	}
	return runWinres("magefiles/winres/winres-gcoderamp.json", "gcoderamp.exe")
}

func Pcb2gcodewrap() error {
	err := sh.Run("go", "build", "./cmd/pcb2gcodewrap")
	if err != nil {
		return err
	}
	return runWinres("magefiles/winres/winres-pcb2gcodewrap.json", "pcb2gcodewrap.exe")
}

func Spoilboard() error {
	err := sh.Run("go", "build", "./cmd/spoilboard")
	if err != nil {
		return err
	}
	return runWinres("magefiles/winres/winres-spoilboard.json", "spoilboard.exe")
}

func GCodeSplit() error {
	err := sh.Run("go", "build", "./cmd/gcodesplit")
	if err != nil {
		return err
	}
	return runWinres("magefiles/winres/winres-gcodesplit.json", "gcodesplit.exe")
}

func runWinres(jsonpath, exepath string) error {
	goos := os.Getenv("GOOS")
	if goos == "" {
		goos = runtime.GOOS
	}
	if goos != "windows" {
		return nil
	}

	return sh.Run("go", "run", "github.com/tc-hib/go-winres", "patch", "--in", jsonpath, "--no-backup", exepath)
}

func Check(ctx context.Context) error {
	r := NewRunner(ctx)
	r.Run("go", "vet", "./...")
	r.Run("go", "run", "golang.org/x/vuln/cmd/govulncheck", "./...")
	r.Run("go", "run", "honnef.co/go/tools/cmd/staticcheck", "./...")
	return r.Wait()
}

// Runner allows running a group of commands sequentially, stopping at the first
// failure.
// See https://github.com/magefile/mage/issues/455 for the feature request
// to include this in Mage.
type Runner struct {
	group *errgroup.Group
	ctx   context.Context
}

// NewRunner constructs a new runner that's bound to the given context. If the
// context is done, no new command will be executed. It does NOT abort an
// already-running command.
func NewRunner(ctx context.Context) *Runner {
	group, groupctx := errgroup.WithContext(ctx)
	group.SetLimit(1)

	return &Runner{
		group: group,
		ctx:   groupctx,
	}
}

// Run the given command.
// This only runs a command if no previous command has failed yet.
func (r *Runner) Run(cmd string, args ...string) {
	r.group.Go(func() error {
		if err := r.ctx.Err(); err != nil {
			return err
		}
		return sh.Run(cmd, args...)
	})
}

// Wait for the commands to finish running, and return any error.
func (r *Runner) Wait() error {
	return r.group.Wait()
}
