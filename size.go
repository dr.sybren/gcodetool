package gcodetool

import (
	"math"

	"github.com/256dpi/gcode"
)

type SizeInfo struct {
	Xmin, Xmax float64
	Ymin, Ymax float64
	Zmin, Zmax float64
}

func (si SizeInfo) Width() float64 {
	return si.Xmax - si.Xmin
}

func (si SizeInfo) Height() float64 {
	return si.Ymax - si.Ymin
}

func (si SizeInfo) Elevation() float64 {
	return si.Zmax - si.Zmin
}

func Size(lines []gcode.Line) SizeInfo {
	size := SizeInfo{
		Xmin: math.Inf(1), Xmax: math.Inf(-1),
		Ymin: math.Inf(1), Ymax: math.Inf(-1),
		Zmin: math.Inf(1), Zmax: math.Inf(-1),
	}

	for lineIdx := range lines {
		for _, code := range lines[lineIdx].Codes {
			switch code.Letter {
			case "X":
				size.Xmin = math.Min(size.Xmin, code.Value)
				size.Xmax = math.Max(size.Xmax, code.Value)
			case "Y":
				size.Ymin = math.Min(size.Ymin, code.Value)
				size.Ymax = math.Max(size.Ymax, code.Value)
			case "Z":
				size.Zmin = math.Min(size.Zmin, code.Value)
				size.Zmax = math.Max(size.Zmax, code.Value)
			}
		}
	}

	return size
}
