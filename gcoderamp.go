package gcodetool

import (
	"fmt"
	"path/filepath"

	"github.com/256dpi/gcode"
	"github.com/rs/zerolog/log"
)

const (
	numRampSteps      = 4    // Number of steps in the ramp.
	waitSeconds       = 0.25 // Wait time per step.
	spindleRampSuffix = "-spindleramp"
)

func SpindleRampifyFile(filename string) (string, error) {
	gcodeFile, err := ParseFile(filename)
	if err != nil {
		return "", fmt.Errorf("reading from %s: %w", filename, err)
	}

	log.Info().Str("file", filename).Msg("adding spindle motor ramps")
	processedFile := insertSpindleRamps(gcodeFile)

	outpath := getOutputPath(filename)
	if err := WriteFile(outpath, processedFile); err != nil {
		return "", fmt.Errorf("writing to %s: %w", outpath, err)
	}
	return outpath, nil
}

// SpindleRamp returns GCode to gradually change the spindle speed.
func SpindleRamp(fromSpeed, toSpeed float64, steps int, includeM3 bool) []gcode.Line {
	delta := (toSpeed - fromSpeed) / float64(steps)
	lines := []gcode.Line{
		{Comment: fmt.Sprintf(" Ramping spindle from %s to %s", FormatNum(fromSpeed), FormatNum(toSpeed))},
	}
	for i := 1; i <= steps; i++ {
		speed := fromSpeed + float64(i)*delta
		lines = append(lines, Line(S(speed)))
		if includeM3 && i == 1 {
			// First S-command should be followed by an M3 to turn the spindle on.
			lines = append(lines, Line(M(3)))
		}
		lines = append(lines, Line(Wait(waitSeconds)...))
	}

	lines = append(lines, gcode.Line{Comment: " end of speed ramp"})
	return lines
}

func insertSpindleRamps(gcodeFile *gcode.File) *gcode.File {
	var (
		setSpindleSpeed float64
		curSpindleSpeed float64
	)

	outLines := []gcode.Line{}

	for lineIdx, line := range gcodeFile.Lines {
		outLine := gcode.Line{Comment: line.Comment}

		for _, code := range line.Codes {
			outCode := code

			logger := log.With().
				Int("line", lineIdx+1).
				Logger()

			switch code.Letter {
			case "S":
				setSpindleSpeed = code.Value
				if setSpindleSpeed == curSpindleSpeed {
					break
				}
				if curSpindleSpeed > 0 && setSpindleSpeed > curSpindleSpeed {
					// Increasing speed while spindle is running. This replaces the input S command.
					logger.Debug().
						Float64("Sfrom", curSpindleSpeed).
						Float64("Sto", setSpindleSpeed).
						Msg("generating spindle ramp")

					ramp := SpindleRamp(curSpindleSpeed, setSpindleSpeed, numRampSteps, false)
					outLines = append(outLines, ramp...)
				} else {
					logger.Debug().
						Float64("Sfrom", curSpindleSpeed).
						Float64("Sto", setSpindleSpeed).
						Msg("tracking spindle speed change")
				}
			case "M":
				switch code.Value {
				case 3:
					if setSpindleSpeed > curSpindleSpeed {
						ramp := SpindleRamp(curSpindleSpeed, setSpindleSpeed, numRampSteps, true)
						outLines = append(outLines, ramp...)
						// The M-code is removed from the current line, as it's already part of the ramp.
						outCode = gcode.GCode{}
					}
					logger.Debug().
						Float64("Sfrom", curSpindleSpeed).
						Float64("Sto", setSpindleSpeed).
						Msg("motor ON")
					curSpindleSpeed = setSpindleSpeed
				case 2, 5, 30:
					curSpindleSpeed = 0
					logger.Debug().Msg("motor OFF")
				case 4:
					logger.Fatal().Msg("M4 is not supported")
				}
			}
			if CodeHasValue(outCode) {
				outLine.Codes = append(outLine.Codes, outCode)
			}
		}
		outLines = append(outLines, outLine)
	}

	if curSpindleSpeed > 0 {
		outLines = append(outLines, Line(
			M(5),
			Comment(" motor was left running, gcodetool turns it off "),
		))
	}

	return &gcode.File{Lines: outLines}
}

func getOutputPath(inputPath string) string {
	ext := filepath.Ext(inputPath)
	return inputPath[:len(inputPath)-len(ext)] + spindleRampSuffix + ext
}

func CodeHasValue(code gcode.GCode) bool {
	return !(code.Comment == "" && code.Letter == "")
}
