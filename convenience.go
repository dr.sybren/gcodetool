package gcodetool

import (
	"fmt"
	"strings"

	"github.com/256dpi/gcode"
)

func Lines(codes ...gcode.GCode) []gcode.Line {
	lines := make([]gcode.Line, 0, len(codes))
	for _, code := range codes {
		lines = append(lines, Line(code))
	}
	return lines
}

func Line(codes ...gcode.GCode) gcode.Line {
	return gcode.Line{Codes: codes}
}

func Comment(comment string) gcode.GCode {
	return gcode.GCode{Comment: comment}
}

func CommentLines(comments ...string) []gcode.Line {
	lines := []gcode.Line{}
	for idx := range comments {
		line := Line(Comment(comments[idx]))
		lines = append(lines, line)
	}
	return lines
}

func M(value float64) gcode.GCode {
	return gcode.GCode{Letter: "M", Value: value}
}

func S(speed float64) gcode.GCode {
	return gcode.GCode{Letter: "S", Value: speed}
}

func G(number int) gcode.GCode {
	return gcode.GCode{Letter: "G", Value: float64(number)}
}

func Z(value float64) gcode.GCode {
	return gcode.GCode{Letter: "Z", Value: value}
}

func Wait(seconds float64) []gcode.GCode {
	return []gcode.GCode{
		{Letter: "G", Value: 4},
		{Letter: "P", Value: seconds},
	}
}

// Splice splices `inject` into `list` before the item with index `index`.
func Splice[T any](list, inject []T, index int) []T {
	result := make([]T, 0, len(list)+len(inject))
	result = append(result, list[:index]...)
	result = append(result, inject...)
	result = append(result, list[index:]...)
	return result
}

// FormatNum returns a string representation of the given number.
func FormatNum(number float64) string {
	return strings.TrimRight(strings.TrimRight(fmt.Sprintf("%f", number), "0"), ".")
}
