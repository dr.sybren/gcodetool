package gcodetool

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestSizeInfo(t *testing.T) {
	si := SizeInfo{}
	assert.Equal(t, 0.0, si.Width())
	assert.Equal(t, 0.0, si.Height())

	si = SizeInfo{
		Xmin: 3.14,
		Xmax: 94.1,
		Ymin: -2.72,
		Ymax: 16.1,
		Zmin: -0.1,
		Zmax: 3,
	}
	assert.Equal(t, 90.96, si.Width())
	assert.Equal(t, 18.82, si.Height())
	assert.Equal(t, 3.1, si.Elevation())
}

func TestSize(t *testing.T) {
	file, err := ParseFile("testfiles/size.ngc")
	require.NoError(t, err)

	size := Size(file.Lines)
	expect := SizeInfo{
		Xmin: 0.81530,
		Xmax: 8.96654,
		Ymin: 1.39010,
		Ymax: 11.11929,
		Zmin: -0.050,
		Zmax: 10.000,
	}
	assert.Equal(t, expect, size)
}
