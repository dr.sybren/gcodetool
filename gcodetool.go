package gcodetool

import (
	"fmt"
	"os"

	"github.com/256dpi/gcode"
)

// Candle uses .ngc, FlatCam uses .ncg, both agree on .ncc.
// And it's a nice Star Trek reference.
const GCodeFileExt = ".ncc"

// ParseFile loads and parses a GCode file.
func ParseFile(path string) (*gcode.File, error) {
	source, err := os.Open(path)
	if err != nil {
		return nil, fmt.Errorf("opening file: %w", err)
	}
	defer source.Close()

	gcodeFile, err := gcode.ParseFile(source)
	if err != nil {
		return nil, fmt.Errorf("parsing gcode: %w", err)
	}

	if err := source.Close(); err != nil {
		return nil, fmt.Errorf("closing file: %w", err)
	}
	return gcodeFile, nil
}

// WriteFile writes gcode to a file on disk.
func WriteFile(path string, gcodeFile *gcode.File) error {
	outfile, err := os.Create(path)
	if err != nil {
		return fmt.Errorf("creating file: %w", err)
	}
	defer outfile.Close()

	if err := gcode.WriteFile(outfile, gcodeFile); err != nil {
		return fmt.Errorf("writing gcode: %w", err)
	}

	if err := outfile.Close(); err != nil {
		return fmt.Errorf("closing file: %w", err)
	}
	return nil
}
