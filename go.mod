module gitlab.com/dr.sybren/gcodetool

go 1.19

require (
	github.com/256dpi/gcode v0.3.0
	github.com/magefile/mage v1.14.0
	github.com/mattn/go-colorable v0.1.12
	github.com/rs/zerolog v1.28.0
	github.com/stretchr/testify v1.8.0
	github.com/tc-hib/go-winres v0.3.1
	golang.org/x/sync v0.1.0
	golang.org/x/vuln v0.0.0-20230201222900-4c848edceff1
	gopkg.in/yaml.v2 v2.2.8
	honnef.co/go/tools v0.4.0
)

require (
	github.com/BurntSushi/toml v1.2.1 // indirect
	github.com/cpuguy83/go-md2man/v2 v2.0.0 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646 // indirect
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/tc-hib/winres v0.1.6 // indirect
	github.com/urfave/cli/v2 v2.3.0 // indirect
	golang.org/x/exp v0.0.0-20220722155223-a9213eeb770e // indirect
	golang.org/x/exp/typeparams v0.0.0-20221208152030-732eee02a75a // indirect
	golang.org/x/image v0.0.0-20210220032944-ac19c3e999fb // indirect
	golang.org/x/mod v0.7.0 // indirect
	golang.org/x/sys v0.4.0 // indirect
	golang.org/x/tools v0.5.1-0.20230117180257-8aba49bb5ea2 // indirect
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
