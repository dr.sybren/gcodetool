# Settings

This is a collection of the settings I used for my SainSmart Genmitsu 3018
PROVer v2 cnc mill. There's many people who (for good reason) say things very
much depend on the specifics of your cnc machine, the bits you use, the material
you cut, etc.. Because of this I actually had a hard time figuring out what to
do when I started as a newbie in the cnc world. Here are my settings. They may
not apply to your situation at all, but I hope that having a concrete example
helps anyway.

## GRBL settings

```
[CTRL+X] < Grbl 1.1f ['$' for help]
Grbl for ARM32
Version:ARM32 V2.1
$0=10 (step pulse,usec)
$1=25 (step idle delay,msec)
$2=0 (stepport invert mask)
$3=2 (dirport invert mask)
$4=0 (stepenable invert,bool)
$5=1 (lims pin invert,bool)
$6=1 (probe pin invert,bool)
$10=1 (status report mask)
$11=0.010 (junction deviation)
$12=0.002 (arc tolerance,mm)
$13=0 (report inches,bool)
$20=1 (soft limits,bool)
$21=1 (hard limits,bool)
$22=1 (home cycle,bool)
$23=3 (homing dir invert mask)
$24=100.000 (homing feed,mm/min)
$25=1000.000 (homing seek,mm/min)
$26=250 (homing debounce,msec)
$27=2.000 (homing pull-off,mm)
$30=20000 (maximum spindle speed,rpm)
$31=0 (minimum spindle speed,rpm)
$32=0 (laser mode enable,bool)
$100=800.000 (x axis pulse:step/mm)
$101=800.000 (y axis pulse:step/mm)
$102=800.000 (z axis pulse:step/mm)
$110=4000.000 (x axis max rata:mm/min)
$111=4000.000 (y axis max rata:mm/min)
$112=4000.000 (z axis max rata:mm/min)
$120=150.000 (x axis acceleration:mm/s^2)
$121=150.000 (y axis acceleration:mm/s^2)
$122=100.000 (z axis acceleration:mm/s^2)
$130=283.000 (x aixs max travel:mm)
$131=178.000 (y aixs max travel:mm)
$132=100.000 (z aixs max travel:mm)
```

## Custom Commands

I use these as 'custom command' buttons in [Candle][candle]:

| Label           | GCode             |
|-----------------|-------------------|
| Zero XY         | `G92 X0 Y0`       |
| Zero Z          | `G92 Z0`          |
| Home            | `$H`              |
|                 | `G92 X0 Y0`       |
|                 | `G28`             |
| Go to Z=top     | `G53 G0 Z0`       |
| Go to XY origin | `G90 G0 X0 Y0`    |
| Probe to 12mm   | `G90`             |
|                 | `G21`             |
|                 | `G38.2 Z-35 F100` |
|                 | `G92 Z12.00 `     |
|                 | `G0 Z20`          |
| Go to Z=1       | `G21 G0 Z1`       |
| Go to Z=10      | `G21 G0 Z10`      |
| Present Work    | `G53 G0 Z0`       |
|                 | `G53 G0 Y178`     |

Probe to PCB:
```gcode
G90
G21
(ENSURE PROBE CONNECTED)
(ENSURE PROBE CONNECTED)
(ENSURE PROBE CONNECTED)
M0
G38.2 Z-35 F100
G92 Z0
G0 Z1
G38.2 Z-35 F25
G92 Z0
G0 Z3
G4 P0
```

[candle]: https://github.com/Denvi/Candle/

## EasyEDA

Ground plane:

| Setting             | Value  |
|---------------------|--------|
| Clearance           | 0.3 mm |
| Keep Islands        | Yes    |
| Improve Fabrication | No     |

## Spoilboard

Holes at:

```
; X-line
<Idle|MPos:5.500,51.055,-36.000|FS:0,0>
<Idle|MPos:50.229,51.055,-36.000|FS:0,0>
<Idle|MPos:110.628,51.055,-36.000|FS:0,0>
<Idle|MPos:175.830,51.055,-36.000|FS:0,0>
<Idle|MPos:235.834,51.055,-36.000|FS:0,0>
<Idle|MPos:280.936,51.055,-36.000|FS:0,0>

; Y-line
<Idle|MPos:175.830,10.755,-36.000|FS:0,0>
<Idle|MPos:175.830,51.055,-36.000|FS:0,0>
<Idle|MPos:175.830,91.000,-36.000|FS:0,0>
<Idle|MPos:175.830,131.000,-36.000|FS:0,0>
<Idle|MPos:175.830,171.000,-36.000|FS:0,0>
```

## Spoilboard usable area

Top-left corner is at `G53 G0 X56.3 Y126.6 Z-29`
Or in negative space: `G53 G0 X-223.8 Y125.9 Z-28`

Z-coordinate is then with V-bit ~1mm above the PCB surface.

