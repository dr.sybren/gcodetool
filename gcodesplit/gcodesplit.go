package gcodesplit

import (
	"github.com/256dpi/gcode"
	"github.com/rs/zerolog/log"
	"gitlab.com/dr.sybren/gcodetool"
)

var (
	programEnd = []gcode.Line{
		gcodetool.Line(gcode.GCode{Letter: "M", Value: 5}, gcode.GCode{Comment: "Spindle off"}),
		gcodetool.Line(gcode.GCode{Letter: "M", Value: 9}, gcode.GCode{Comment: "Coolant off"}),
		gcodetool.Line(gcode.GCode{Letter: "M", Value: 2}, gcode.GCode{Comment: "Program end"}),
	}
)

func SplitPerTool(gcodeFile *gcode.File, outFilePrefix string) error {
	header, body := splitHeader(gcodeFile.Lines)

	var (
		state           GCodeState
		identifiedTools map[string]*ToolWriter = make(map[string]*ToolWriter)
		seenFirstTool   bool
	)

	// Always start with an anonymous tool.
	currentTool := NewToolWriter(outFilePrefix)
	go currentTool.Run()

	for _, line := range body {
		// Update the state seen so far, as this has to be ensured to be consistent in new files.
		state = state.Update(line)

		// See if we need to output to a new file.
		switch {
		case hasCode(line, "T"):
			// This starts a new tool, which is currently still unidentified.
			if currentTool.IsAnonymous() {
				if seenFirstTool {
					log.Warn().
						Stringer("line", &line).
						Msg("found new tool start, but previous tool was still unidentified")
				} else {
					seenFirstTool = true
				}
			} else {
				log.Debug().
					Stringer("line", &line).
					Msg("found new tool start")
				currentTool = NewToolWriter(outFilePrefix)
				go currentTool.Run()
			}
			continue
		case hasCode(line, "M", 0):
			// This is the 'pause' for the actual tool change. This gets replaced by
			// actually writing to a new file, so it shouldn't be written out.
			continue
		}

		if ident, found := IdentifyTool(line); found {
			// This is a comment like (MSG, Change tool bit to mill diameter 0.50000mm)
			logger := log.With().
				Str("tool", ident.Identifier).
				Logger()

			// See if we already know this tool. If so, it should be merged.
			if knownTool, found := identifiedTools[ident.Identifier]; found {
				logger.Debug().Msg("already knew this tool, merging")
				currentTool.WriteBufferTo(knownTool)
				if err := currentTool.Close(); err != nil {
					logger.Error().Err(err).Msg("error closing unidentified tool")
				}
				currentTool = knownTool
			} else {
				logger.Debug().Msg("new tool identified")
				currentTool.IdentifyTool(ident.Identifier)
				identifiedTools[ident.Identifier] = currentTool

				currentTool.Chan() <- header
			}

			currentTool.SetState(state)
		}

		currentTool.Chan() <- []gcode.Line{line}
	}

	// Send the 'program end' lines.
	for _, tw := range identifiedTools {
		if tw == currentTool {
			// Don't include the 'end program' stuff to the last-seen tool,
			// as that's already sent as part of the input GCode.
			continue
		}

		tw.Chan() <- programEnd
	}

	// Close the tool writers.
	for _, tw := range identifiedTools {
		tw.Close()
	}

	return nil
}

func splitHeader(lines []gcode.Line) (header []gcode.Line, body []gcode.Line) {
	for lineIdx := range lines {
		for _, code := range lines[lineIdx].Codes {
			if code.Letter != "" {
				// Found a real code, so this is the first line of the body.
				return lines[:lineIdx], lines[lineIdx:]
			}
		}
	}
	return lines, nil
}

func hasCode(line gcode.Line, letter string, value ...float64) bool {
	if len(line.Codes) == 0 {
		return false
	}
	for _, code := range line.Codes {
		if code.Letter != letter {
			continue
		}
		if len(value) == 0 { // No value to compare to.
			return true
		}
		if code.Value == value[0] { // Correct value.
			return true
		}
	}
	return false
}
