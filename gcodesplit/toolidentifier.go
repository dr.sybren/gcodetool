package gcodesplit

import (
	"regexp"
	"strings"

	"github.com/256dpi/gcode"
)

var toolIdentRe = regexp.MustCompile("^MSG, Change tool bit to (.*)$")

type ToolIdentifier struct {
	Identifier string
	Comment    string
}

func IdentifyTool(line gcode.Line) (ToolIdentifier, bool) {
	if len(line.Codes) == 0 || !strings.HasPrefix(line.Codes[0].Comment, "MSG,") {
		return ToolIdentifier{}, false
	}
	// This is a comment like (MSG, Change tool bit to mill diameter 0.50000mm)
	comment := line.Codes[0].Comment
	ident := toolIdentRe.FindStringSubmatch(comment)
	if ident == nil {
		return ToolIdentifier{}, false
	}

	ti := ToolIdentifier{
		Identifier: ident[1],
		Comment:    comment,
	}
	return ti, true
}
