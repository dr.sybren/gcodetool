package gcodesplit

import (
	"fmt"
	"io"
	"os"
	"sync"
	"time"

	"github.com/256dpi/gcode"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.com/dr.sybren/gcodetool"
)

const unknownIdentifier = "unknown"

type ToolWriter struct {
	identifier   string
	isIdentified bool

	lineBuffer      []gcode.Line
	numLinesWritten int

	inputChan chan []gcode.Line
	done      chan struct{}

	outPathPrefix string
	outputFile    *os.File

	lastWrittenState *GCodeState
	mutex            sync.Mutex
}

func NewToolWriter(outPathPrefix string) *ToolWriter {
	return &ToolWriter{
		identifier:    unknownIdentifier,
		outPathPrefix: outPathPrefix,
		inputChan:     make(chan []gcode.Line),
		done:          make(chan struct{}),
	}
}

func (tw *ToolWriter) Chan() chan<- []gcode.Line {
	return tw.inputChan
}

func (tw *ToolWriter) Close() error {
	close(tw.inputChan)
	select {
	case <-time.After(2 * time.Second):
		panic(fmt.Sprintf("tool writer %q took too long to close", tw.identifier))
	case <-tw.done: // Wait until the tool writer is done.
	}

	if tw.outputFile == nil {
		return nil
	}

	err := tw.outputFile.Close()
	if err != nil {
		return fmt.Errorf("closing file: %w", err)
	}

	logger := tw.logger()
	logger.Info().
		Int("lines", tw.numLinesWritten).
		Msg("closed file")

	return nil
}

func (tw *ToolWriter) Run() {
	log.Debug().Msg("tool writer starting")

	defer close(tw.done)
	defer func() {
		// Defer evaluation of tw.identifier.
		log.Debug().
			Str("identifier", tw.identifier).
			Msg("tool writer stopping")
	}()

	for lines := range tw.inputChan {
		if tw.outputFile == nil {
			// Nothing to write to yet -- buffer these lines.
			tw.bufferLines(lines)
		} else {
			tw.writeLines(lines)
		}
	}
}

func (tw *ToolWriter) IdentifyTool(identifier string) error {
	tw.mutex.Lock()
	defer tw.mutex.Unlock()

	if tw.isIdentified && tw.identifier != identifier {
		logger := tw.logger()
		logger.Panic().Str("newIdentifier", identifier).
			Msg("already-identified tool writer got new identification")
	}
	tw.identifier = identifier
	tw.isIdentified = true

	if tw.outputFile != nil {
		// File already open, this function is done.
		return nil
	}

	filename := fmt.Sprintf("%s-%s%s",
		tw.outPathPrefix, identifier, gcodetool.GCodeFileExt)

	logger := tw.logger()
	logger.Info().Str("filename", filename).Msg("opening file for tool")

	file, err := os.Create(filename)
	if err != nil {
		return err
	}
	tw.outputFile = file
	return nil
}

func (tw *ToolWriter) IsAnonymous() bool {
	return !tw.isIdentified
}

func (tw *ToolWriter) Identifier() string {
	return tw.identifier
}

func (tw *ToolWriter) SetState(state GCodeState) {
	tw.mutex.Lock()
	logger := tw.logger()

	if tw.lastWrittenState != nil && tw.lastWrittenState.Equals(state) {
		logger.Debug().Msg("new state is same as previous state, ignoring")
		tw.mutex.Unlock()
		return
	}

	lines := state.Reconstruct()
	tw.lastWrittenState = &state

	logger.Debug().
		Interface("state", state).
		Msg("new state stored, sending to output")
	tw.mutex.Unlock() // otherwise inputChan will block.
	tw.inputChan <- lines
}

// WriteBufferTo sends the buffered lines to the other ToolWriter.
// This function can only be used if this ToolWriter is unidentified.
func (tw *ToolWriter) WriteBufferTo(other *ToolWriter) error {
	tw.mutex.Lock()
	defer tw.mutex.Unlock()

	if tw.isIdentified {
		return fmt.Errorf("tool writer %q is already identified", tw.identifier)
	}

	if len(tw.lineBuffer) == 0 {
		return nil
	}

	other.Chan() <- tw.lineBuffer
	tw.lineBuffer = make([]gcode.Line, 0)

	return nil
}

func (tw *ToolWriter) bufferLines(lines []gcode.Line) {
	tw.mutex.Lock()
	defer tw.mutex.Unlock()

	log.Trace().
		Str("identifier", tw.identifier).
		Int("numLines", len(lines)).
		Msg("buffering lines")
	tw.lineBuffer = append(tw.lineBuffer, lines...)
}

func (tw *ToolWriter) writeLines(lines []gcode.Line) {
	if len(tw.lineBuffer) > 0 {
		tw.mutex.Lock()
		buffered := tw.lineBuffer
		tw.lineBuffer = make([]gcode.Line, 0)
		tw.mutex.Unlock()

		tw.writeLines(buffered)
	}

	log.Trace().
		Str("identifier", tw.identifier).
		Int("numLines", len(lines)).
		Msg("writing lines")

	for _, line := range lines {
		_, err := io.WriteString(tw.outputFile, line.String())
		if err != nil {
			logger := tw.logger()
			logger.Panic().
				Err(err).
				Str("filename", tw.outputFile.Name()).
				Stringer("line", &line).
				Msg("error writing to file")
		}
	}

	tw.numLinesWritten += len(lines)
}

func (tw *ToolWriter) logger() zerolog.Logger {
	return log.With().Str("tool", tw.identifier).Logger()
}
