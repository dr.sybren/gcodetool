package gcodesplit

import (
	"fmt"
	"reflect"

	"github.com/256dpi/gcode"
	"gitlab.com/dr.sybren/gcodetool"
)

type GCodeState struct {
	FeedRate    float64 // F
	SpindleRate float64 // S

	IsImperial bool // Set by G20, cleared by G21
	IsAbsolute bool // Set by G90, cleared by G91
	SeenG94    bool // Set by G94, not sure what clears it
}

func (s GCodeState) Reconstruct() []gcode.Line {
	line := func(letter string, value float64, comment ...string) gcode.Line {
		code := gcode.GCode{Letter: letter, Value: value}
		if len(comment) == 0 {
			return gcodetool.Line(code)
		}
		return gcodetool.Line(code, gcode.GCode{Comment: comment[0]})
	}

	var unitCode, absRelCode float64
	var unitComment, unit, absRelComment string
	if s.IsImperial {
		unitCode = 20
		unit = "something imperial"
		unitComment = "Units == something imperial"
	} else {
		unitCode = 21
		unit = "mm"
		unitComment = "Units == mm"
	}
	if s.IsAbsolute {
		absRelCode = 90
		absRelComment = "Absolute coordinates"
	} else {
		absRelCode = 91
		absRelComment = "Relative coordinates"
	}

	lines := []gcode.Line{
		{Comment: " Reconstructed GRBL state"},
		line("G", unitCode, unitComment),
		line("G", absRelCode, absRelComment),
	}
	if s.SeenG94 {
		lines = append(lines, line("G", 94, fmt.Sprintf("Feed rate in %s/min", unit)))
	}
	if s.FeedRate > 0 {
		lines = append(lines, line("F", s.FeedRate, "Feed Rate"))
	}
	if s.SpindleRate > 0 {
		lines = append(lines, line("S", s.SpindleRate, "RPM spindle speed"))
	}
	lines = append(lines,
		gcode.Line{Comment: " End of reconstructed GRBL state"},
		gcode.Line{},
	)
	return lines
}

func (s GCodeState) Equals(other GCodeState) bool {
	return reflect.DeepEqual(s, other)
}

func (s GCodeState) Update(line gcode.Line) GCodeState {
	for _, code := range line.Codes {
		switch code.Letter {
		case "F":
			s.FeedRate = code.Value
		case "S":
			s.SpindleRate = code.Value
		case "G":
			switch code.Value {
			case 20:
				s.IsImperial = true
			case 21:
				s.IsImperial = false
			case 90:
				s.IsAbsolute = true
			case 91:
				s.IsAbsolute = false
			case 94:
				s.SeenG94 = true
			}
		}
	}
	return s
}
