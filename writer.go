package gcodetool

import (
	"fmt"
	"io"

	"github.com/256dpi/gcode"
)

// Writer makes it simpler to write GCode to a stream.
type Writer struct {
	stream io.Writer
}

func NewWriter(stream io.Writer) *Writer {
	return &Writer{stream: stream}
}

func (w *Writer) Line(line string) {
	w.stream.Write([]byte(line))
	w.stream.Write([]byte("\n"))
}

func (w *Writer) Linef(format string, args ...interface{}) {
	w.Line(fmt.Sprintf(format, args...))
}

func (w *Writer) RetractToMachineZ(machineZ float64) {
	w.Linef("G53 G0 Z%f", machineZ)
}

func (w *Writer) FastMoveXYZ(x, y, z float64) {
	w.Linef("G0 X%f Y%f Z%f", x, y, z)
}
func (w *Writer) FastMoveXY(x, y float64) {
	w.Linef("G0 X%f Y%f", x, y)
}
func (w *Writer) FastMoveZ(z float64) {
	w.Linef("G0 Z%f", z)
}

func (w *Writer) CutXY(x, y, feedRate float64) {
	w.Linef("G1 X%f Y%f F%f", x, y, feedRate)
}
func (w *Writer) CutZ(z, feedRate float64) {
	w.Linef("G1 Z%f F%f", z, feedRate)
}
func (w *Writer) CutArcCW(x, y, i, j, feedRate float64) {
	w.Linef("G2 X%f Y%f I%f J%f F%f", x, y, i, j, feedRate)
}
func (w *Writer) CutArcCCW(x, y, i, j, feedRate float64) {
	w.Linef("G3 X%f Y%f I%f J%f F%f", x, y, i, j, feedRate)
}

func (w *Writer) Wait() {
	w.Line("G4 P0")
}

func (w *Writer) AbsoluteCoords() {
	w.Line("G90 (absolute coordinates)")
}
func (w *Writer) MetricUnits() {
	w.Line("G21 (metric units)")
}

func (w *Writer) ProgramPause() {
	w.Line("M0")
}
func (w *Writer) ProgramEnd() {
	w.Line("M2")
}
func (w *Writer) SpindleStart() {
	w.Line("M3")
}
func (w *Writer) SpindleStop() {
	w.Line("M5")
}
func (w *Writer) SpindleRamp(fromSpeed, toSpeed int64) {
	lines := SpindleRamp(float64(fromSpeed), float64(toSpeed), 4, true)
	file := gcode.File{Lines: lines}
	gcode.WriteFile(w.stream, &file)
}

func (w *Writer) Comment(text string) {
	w.Linef("; %s", text)
}
func (w *Writer) Commentf(format string, args ...interface{}) {
	w.Linef("; "+format, args...)
}

// ZProbe probes down until contact, then sets Z=shimHeight.
// Be sure to stop the spindle, instruct the user to connect the probe, and
// pause the program!
func (w *Writer) ZProbe(shimHeight float64, finalProbeHeight ...float64) {
	var finalZ float64
	switch len(finalProbeHeight) {
	case 0:
		finalZ = -35.0
	case 1:
		finalZ = finalProbeHeight[0]
	default:
		panic("too many arguments to Writer.ZProbe")
	}

	w.Linef("G38.2 Z%f F100", finalZ)
	w.Linef("G92 Z%f", shimHeight)
	w.FastMoveZ(shimHeight + 1.0)
	w.Linef("G38.2 Z%f F25", finalZ)
	w.Linef("G92 Z%f", shimHeight)
	w.FastMoveZ(shimHeight + 2.0)
	w.Wait()
}
