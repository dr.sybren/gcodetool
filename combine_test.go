package gcodetool

import (
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

const testCombineTempFile = "testfiles/test-combine.ngc"

func TestCombine(t *testing.T) {
	defer os.Remove(testCombineTempFile)

	gcode, err := Combine("testfiles/first.ngc", "testfiles/second.ngc")
	require.NoError(t, err)

	err = WriteFile(testCombineTempFile, gcode)
	require.NoError(t, err)

	actual, err := os.ReadFile(testCombineTempFile)
	require.NoError(t, err)
	expect, err := os.ReadFile("testfiles/expect-combined.ngc")
	require.NoError(t, err)

	assert.Equal(t,
		strings.ReplaceAll(string(expect), "\r\n", "\n"),
		strings.ReplaceAll(string(actual), "\r\n", "\n"),
	)
}
