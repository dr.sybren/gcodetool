package gcodetool

import (
	"fmt"
	"path/filepath"

	"github.com/256dpi/gcode"
)

// Combine multiple GCode programs into one file.
func Combine(srcpath ...string) (*gcode.File, error) {
	var (
		outLines []gcode.Line
		err      error
	)

	for idx, filepath := range srcpath {
		mayEndProgram := idx == len(srcpath)-1
		outLines, err = appendFile(filepath, mayEndProgram, outLines)
		if err != nil {
			return nil, fmt.Errorf("%s: %w", filepath, err)
		}
	}

	outFile := gcode.File{Lines: outLines}
	return &outFile, nil
}

func appendFile(gcodeFile string, mayEndProgram bool, outLines []gcode.Line) ([]gcode.Line, error) {
	file, err := ParseFile(gcodeFile)
	if err != nil {
		return outLines, err
	}

	outLines = append(outLines,
		gcode.Line{Comment: " -------------------------------------------------"},
		gcode.Line{Comment: " " + filepath.Base(gcodeFile)},
	)

	for _, line := range file.Lines {
		if !mayEndProgram && isEndProgram(line) {
			line = gcode.Line{Comment: " Removed program end"}
		}
		outLines = append(outLines, line)
	}

	return outLines, nil
}

func isEndProgram(line gcode.Line) bool {
	// Big fat assumption: if there is an M2, it's first on its line.
	return len(line.Codes) > 0 && line.Codes[0].Letter == "M" && line.Codes[0].Value == 2
}
