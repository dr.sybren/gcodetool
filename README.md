# Dr. Sybren's GCode Toolbox

This is a little tool I wrote to make my life easier when milling PCBs.

**DISCLAIMER:** I wrote this to scratch my own itch. If it's useful for you,
[let me know](https://stuvel.eu/#contact)! If you're experiencing issues,
definitely [file a report](https://gitlab.com/dr.sybren/gcodetool/-/issues).
There is no warranty, no guarantee of any usefulness. There is no customisation
except where I personally need it, so a lot of the parameters are hard-coded to
match my equipment and workflow. To adjust, just create a fork and change
whatever you need, or add code to customise things more easily and send me a
pull request.

At the moment, the toolbox has two tools:

## Spindle Ramp-up

My spindle motor is faster than the original one bundled with my CNC router, and
thus it draws more current. The power supply cannot (comfortably) handle the
peak caused by instantly going to max RPM. Ramping it up gradually (in like a
second, it doesn't take long) makes it much easier for the power supply and the
controller board.

This tool processes a given GCode file, and spits out a new one where all the
spindle motor speed changes are divided into a few steps, creating a ramped RPM
profile.

```
gcoderamp myfile.ngc
```

This will parse `myfile.ngc` and output to `myfile-spindleramp.ngc`.

## PCB2GCode Wrapper

[PCB2GCode][pcb2gcode] is a tool to convert Gerber files to GCode. It's a
commandline (CLI) tool with many options, so it's not the most intuitive to use.
There is an accompanying GUI application, but that's lagging behind the CLI by
several versions. Furthermore, for my workflow I need to invoke it multiple
times in order to remove copper from larger areas, and of course I automated
that.

[pcb2gcode]: https://github.com/pcb2gcode/pcb2gcode/
[391]: https://github.com/pcb2gcode/pcb2gcode/issues/391
[545]: https://github.com/pcb2gcode/pcb2gcode/issues/545

```
pcb2gcodewrap /path/to/dir/with/unzipped/gerber
```

The above command will do the following:

- Create an output directory for the GCode. If the Gerber files sit in a
  directory named `gerber`, it will create a `gcode` directory next to it;
  otherwise it will create a `gcode` directory inside of it. For example:
    - `/path/to/gerber` will create GCode in `/path/to/gcode`
    - `/path/to/my-pbc` will create GCode in `/path/to/my-pcb/gcode`
- Convert the top layer (`Gerber_TopLayer.GTL`) to GCode (`Gerber_TopLayer.ngc`)
- Convert the top assembly layer (`Gerber_TopAssemblyLayer.GTA`) to GCode
  (`Gerber_TopAssemblyLayer.GTA`). This is done in a special way for removing
  areas of copper; see below.
- Process these GCode files with *Spinde Ramp-up* (`{filename}-spindleramp.ngc`).
- Combine the GCode files into a single file that can be sent to the CNC router
  (`combined.ngc`).

**NOTE:** Only the front isolation pass is handled here. I'm new to CNC routers,
and the manufacturer has some stocking issues. Becuse of this I haven't gotten
around to drilling or outlining any PCBs. When I do, I'm sure this tool will
expand.

### Copper Removal

To remove areas of copper, instead of just isolating one area from another, some
special approach is necessary. Because PCB2GCode doesn't deal with this itself
(see issues [#391] and [#545][545]) it needs to be run with some special
options. *PCB2GCode Wrapper* takes care of that for you.

To remove copper, just draw copper areas in the *Top Assembly Layer* of your PCB
design tool. These will be milled out completely.


### GCode Split per Tool

The PCB2GCode Wrapper outputs `combined-front.ncc` and `combined-back.ncc`,
which contain the GCode for the front and back mill operations. These combine
the isolation milling with the copper removal milling (se above).

These files combine all milling tools in one file, pausing for tool changes. In
order to get a separate GCode file per tool, run the file through `gcodesplit`.

## Building

The simplest way to build is:

```sh
go run mage.go build
```

If you want slightly faster builds, install Mage yourself and then use it:

```sh
go install github.com/magefile/mage
mage build
```

## License

This code is licensed under the GPLv3 license, as described in
[the license file](LICENSE).

## Acknowledgements

*Dr. Sybren's GCode Tools* is heavily based on the following projects:

- [pcb2gcode][pcb2gcode] for converting Gerber to GCode files
- [256dpi/gcode](github.com/256dpi/gcode) for GCode parsing
- [magefile/mage](github.com/magefile/mage) for building the executables
- [rs/zerolog](github.com/rs/zerolog) for logging
- [stretchr/testify](github.com/stretchr/testify) for unit tests
