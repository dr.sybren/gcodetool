package gcodetool

import (
	"testing"

	"github.com/256dpi/gcode"
	"github.com/stretchr/testify/assert"
)

func TestSpindleRampWithoutM3(t *testing.T) {
	codes := SpindleRamp(10000, 20000, 4, false)
	expect := []gcode.Line{
		{Comment: " Ramping spindle from 10000 to 20000"},
		Line(S(12500)),
		Line(Wait(waitSeconds)...),
		Line(S(15000)),
		Line(Wait(waitSeconds)...),
		Line(S(17500)),
		Line(Wait(waitSeconds)...),
		Line(S(20000)),
		Line(Wait(waitSeconds)...),
		{Comment: " end of speed ramp"},
	}
	assert.Equal(t, expect, codes)
}

func TestSpindleRampWithM3(t *testing.T) {
	codes := SpindleRamp(0, 20000, 4, true)
	expect := []gcode.Line{
		{Comment: " Ramping spindle from 0 to 20000"},
		Line(S(5000)),
		Line(M(3)),
		Line(Wait(waitSeconds)...),
		Line(S(10000)),
		Line(Wait(waitSeconds)...),
		Line(S(15000)),
		Line(Wait(waitSeconds)...),
		Line(S(20000)),
		Line(Wait(waitSeconds)...),
		{Comment: " end of speed ramp"},
	}
	assert.Equal(t, expect, codes)
}

func TestSplice(t *testing.T) {
	input := []int{0, 1, 2, 3}
	splice := []int{47, 327}

	{
		actual := Splice(input, splice, 2)
		expect := []int{0, 1, 47, 327, 2, 3}
		assert.Equal(t, expect, actual)
	}
	{
		actual := Splice(input, splice, 0)
		expect := []int{47, 327, 0, 1, 2, 3}
		assert.Equal(t, expect, actual)
	}
	{
		actual := Splice(input, splice, 4)
		expect := []int{0, 1, 2, 3, 47, 327}
		assert.Equal(t, expect, actual)
	}
}
