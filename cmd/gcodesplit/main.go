package main

import (
	"flag"
	"os"
	"path/filepath"
	"time"

	"github.com/mattn/go-colorable"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"

	"gitlab.com/dr.sybren/gcodetool"
	"gitlab.com/dr.sybren/gcodetool/cmd"
	"gitlab.com/dr.sybren/gcodetool/gcodesplit"
)

func Main() int {
	parseCliArgs()

	infile := flag.Arg(0)
	if infile == "" {
		log.Error().Msg("Gimme a file!")
		return 1
	}

	logger := log.With().
		Str("infile", infile).
		Logger()

	// Load the input file.
	logger.Info().Msg("loading input file")
	gcodeFile, err := gcodetool.ParseFile(infile)
	if err != nil {
		logger.Error().Err(err).Msg("unable to read file")
		return 2
	}
	logger.Info().Int("numLines", len(gcodeFile.Lines)).Msg("loaded input")

	// Determine output file prefix.
	ext := filepath.Ext(infile)
	outPrefix := infile[:len(infile)-len(ext)]
	logger.Debug().Str("outPrefix", outPrefix).Msg("determined output file path prefix")

	if err := gcodesplit.SplitPerTool(gcodeFile, outPrefix); err != nil {
		logger.Error().Err(err).Msg("error splitting")
	}

	return 0
}

func main() {
	os.Exit(cmd.MainRunner(Main))
}

func parseCliArgs() {
	var quiet, debug, trace bool

	flag.BoolVar(&quiet, "quiet", false, "Only log warning-level and worse.")
	flag.BoolVar(&debug, "debug", false, "Enable debug-level logging.")
	flag.BoolVar(&trace, "trace", false, "Enable trace-level logging.")
	flag.Parse()

	var logLevel zerolog.Level
	switch {
	case trace:
		logLevel = zerolog.TraceLevel
	case debug:
		logLevel = zerolog.DebugLevel
	case quiet:
		logLevel = zerolog.WarnLevel
	default:
		logLevel = zerolog.InfoLevel
	}

	zerolog.SetGlobalLevel(logLevel)
	output := zerolog.ConsoleWriter{Out: colorable.NewColorableStdout(), TimeFormat: time.RFC3339}
	log.Logger = log.Output(output).Level(logLevel)
}
