package cmd

import (
	"bufio"
	"fmt"
	"os"
	"runtime"
	"runtime/debug"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

// MainRunner sets up zerolog and on Windows ensures the executable doesn't pop
// out of existence when it's done running.
func MainRunner(main func() int) int {
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr}).Level(zerolog.InfoLevel)
	startTime := time.Now()

	if runtime.GOOS == "windows" {
		defer func() {
			oops := recover()
			if oops != nil {
				stack := debug.Stack()
				fmt.Println(string(stack))
				if err, ok := oops.(error); ok {
					log.Error().Err(err).Msg("panic!")
				} else {
					log.Error().Interface("oops", oops).Msg("panic!")
				}
			}
			log.Info().Msg("press [enter] to stop")
			_, _ = bufio.NewReader(os.Stdin).ReadBytes('\n')
			if oops != nil {
				panic(oops)
			}
		}()
	}

	errCode := main()

	duration := time.Since(startTime)
	log.Info().Stringer("duration", duration).Msg("Done")
	return errCode
}
