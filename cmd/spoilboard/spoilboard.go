package main

import (
	"math"
	"os"

	"github.com/rs/zerolog/log"
	"gitlab.com/dr.sybren/gcodetool"
	"gitlab.com/dr.sybren/gcodetool/cmd"
)

const (
	// All in mm:
	DrillDiameter     = 3.175
	DrillDepth        = 14.0 // Should be 23mm, but my drill is too short for that.
	DrillVFeed        = 40.0
	DrillSpindleSpeed = 20000

	MillDiameter     = 3.175
	MillSpindleSpeed = 20000
	MillVFeed        = 60.0
	MillHFeed        = 60.0
	MillDepthStep    = 1.0
	MillOverlap      = 0.40 // percent

	HoleShaftDiameter = 6.5
	HoleShaftDepth    = 23
	HoleHeadDiameter  = 11.0
	HoleHeadDepth     = 7.0

	TraverseHeight   = 3.0 // must be at least ZProbeShimHeight
	ZProbeShimHeight = 1.6
)

var (
	// My CnC machine cannot reach the X-coordinate required to mill out the head
	// hole at the extreme X.
	XCoords = []float64{5.500 /* 50.229, 110.628, 175.830 */, 235.834 /*, 280.936 */}
	YCoords = []float64{10.755 /* 51.055, 91.000, 131.000, */, 171.000}
)

func Main() int {
	filename := "spoilboard.ncc"
	logger := log.With().Str("filename", filename).Logger()

	stream, err := os.Create(filename)
	if err != nil {
		logger.Error().Err(err).Msg("Unable to create file")
		return 1
	}
	defer stream.Close()

	w := gcodetool.NewWriter(stream)

	w.AbsoluteCoords()
	w.MetricUnits()

	// Drill head holes:
	w.RetractToMachineZ(0)
	w.Commentf("Load drill d=%.2f", DrillDiameter)
	w.ProgramPause()

	doZProbe(w)
	w.SpindleRamp(0, DrillSpindleSpeed)

	w.Comment("Pre-drilling head holes")
	for _, y := range YCoords {
		for _, x := range XCoords {
			drill(w, x, y, DrillDepth)
		}
	}

	// Mill out the head holes:
	w.RetractToMachineZ(0)
	w.SpindleStop()
	w.FastMoveXY(0, 0)
	w.Commentf("Load endmill d=%.2f", MillDiameter)
	w.ProgramPause()

	doZProbe(w)
	w.SpindleRamp(0, MillSpindleSpeed)

	w.Comment("Milling head holes")
	for _, y := range YCoords {
		for _, x := range XCoords {
			head(w, x, y)
		}
	}

	w.RetractToMachineZ(0)
	w.SpindleStop()
	w.FastMoveXY(50, 170)
	w.Comment("Clean out the holes")
	w.ProgramPause()

	w.SpindleRamp(0, MillSpindleSpeed)

	w.Comment("Milling shafts")
	for _, y := range YCoords {
		for _, x := range XCoords {
			shaft(w, x, y)
		}
	}

	w.RetractToMachineZ(0)
	w.ProgramEnd()

	logger.Info().Msg("Created spoilboard GCode")
	return 0
}

func doZProbe(w *gcodetool.Writer) {
	w.FastMoveZ(TraverseHeight)
	w.FastMoveXY(30, 30)

	w.Commentf("Connect Z-probe")
	w.ProgramPause()

	w.ZProbe(ZProbeShimHeight)

	w.Commentf("Disconnect Z-probe and ensure spindle is free")
	w.ProgramPause()
}

func drill(w *gcodetool.Writer, x, y, depth float64) {
	w.FastMoveXY(x, y)
	w.FastMoveZ(TraverseHeight)
	w.CutZ(-depth, DrillVFeed)
	w.FastMoveZ(TraverseHeight)
}

func head(w *gcodetool.Writer, x, y float64) {
	outerRadius := HoleHeadDiameter/2 - MillDiameter/2
	innerRadius := DrillDiameter / 2

	w.Commentf("Mill head hole at X%f Y%f  R=%f", x, y, outerRadius)

	w.FastMoveXY(x, y)
	w.Wait()
	w.FastMoveZ(1.0)
	millCylinder(w, x, y, 0.0, -HoleHeadDepth, innerRadius, outerRadius)
	w.FastMoveZ(TraverseHeight)
}

func shaft(w *gcodetool.Writer, x, y float64) {
	w.Commentf("  Mill shaft hole at X%f Y%f", x, y)

	outerRadius := HoleShaftDiameter/2 - MillDiameter/2
	innerRadius := DrillDiameter / 4

	w.FastMoveXY(x, y)
	w.Wait()
	w.FastMoveZ(-HoleHeadDepth + 1.0)
	millCylinder(w, x, y, -HoleHeadDepth, -HoleShaftDepth, innerRadius, outerRadius)
	w.FastMoveZ(TraverseHeight)
}

func millCylinder(w *gcodetool.Writer, x, y, startZ, endZ, startRadius, endRadius float64) {
	delta := endZ - startZ
	numCircles := int(math.Ceil(-delta / MillDepthStep))
	stepSize := delta / float64(numCircles)

	w.Commentf("    Circle down in %d steps of %f mm each", numCircles, stepSize)

	for step := 0; step < numCircles; step++ {
		millZ := startZ + float64(step+1)*stepSize
		millDisc(w, x, y, millZ, startRadius, endRadius)
	}
}

func millDisc(w *gcodetool.Writer, x, y, z, startRadius, endRadius float64) {
	distance := endRadius - startRadius
	maxStepDistance := MillDiameter * (1 - MillOverlap)
	numSteps := int(math.Ceil(math.Abs(distance / maxStepDistance)))
	stepDistance := distance / float64(numSteps)
	w.Commentf("      Mill z=%f; startRadius = %f; endRadius = %f; steps = %d", z, startRadius, endRadius, numSteps)

	for step := 1; step < numSteps+1; step++ {
		radius := startRadius + float64(step)*stepDistance
		millCircle(w, x, y, z, radius)
	}
}

func millCircle(w *gcodetool.Writer, x, y, z, radius float64) {
	w.Commentf("      Circle x=%f y=%f r=%f", x, y, radius)
	startX := x - radius
	w.CutXY(startX, y, MillHFeed)
	w.CutZ(z, MillVFeed)
	w.CutArcCW(startX, y, radius, 0.0, MillHFeed)
}

func main() {
	os.Exit(cmd.MainRunner(Main))
}
