package main

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"github.com/rs/zerolog/log"

	"gitlab.com/dr.sybren/gcodetool"
	"gitlab.com/dr.sybren/gcodetool/cmd"
	"gitlab.com/dr.sybren/gcodetool/gerber"
	"gitlab.com/dr.sybren/gcodetool/tempzip"
)

const configFilename = "pcb2gcodewrap.yaml"

type InOutSpec struct {
	gerberInDir string
	gcodeOutDir string
	tempZipper  *tempzip.TempZipper
}

func Main() int {
	cfg, err := loadConfig()
	switch {
	case errors.Is(err, os.ErrNotExist):
		log.Warn().Str("path", configFilename).Msg("could not load configuration file, going to write you a new one")
		cfg, err = gerber.WriteDefaultConfig(configFilename)
		if err != nil {
			log.Error().Err(err).Msg("unable to save default configuration")
			return 10
		}
	case err != nil:
		log.Error().Err(err).Msg("unable to load configuration")
		return 11
	}

	if len(os.Args) < 2 {
		log.Error().Msg("Gimme a directory or ZIP file!")
		return 1
	}

	// Figure out which files to work on, potentially un-ZIPping the input.
	filespec := inOutSpec(cfg.Meta.KeepExtractedZip)
	defer filespec.Close()

	logger := log.With().
		Str("gerberInDir", filespec.gerberInDir).
		Str("gcodeOutDir", filespec.gcodeOutDir).
		Logger()

	logger.Info().Msg("finding Gerber files")
	files, err := gerber.FindFiles(filespec.gerberInDir)
	if err != nil {
		logger.Error().Err(err).Msg("finding gerber files")
		return 2
	}

	logger.Debug().Msgf("found: %#v", files)

	if err := os.MkdirAll(filespec.gcodeOutDir, 0o777); err != nil {
		logger.Error().Err(err).Msg("creating output directory")
		return 3
	}

	gcodeFiles, err := gerber.PCB2GCode(filespec.gcodeOutDir, files, cfg)
	if err != nil {
		logger.Error().Err(err).Msg("running pcb2gcode")
		return 4
	}

	logger.Info().Msg("running gcoderamp on generated files")
	if err := processAndCombine("front", gcodeFiles.AllFrontIsolation(), filespec.gcodeOutDir, cfg); err != nil {
		return 5
	}
	if err := processAndCombine("back", gcodeFiles.AllBackIsolation(), filespec.gcodeOutDir, cfg); err != nil {
		return 8
	}

	if err := processDrill(gcodeFiles, filespec.gcodeOutDir); err != nil {
		return 6
	}
	if err := processOutline(gcodeFiles, filespec.gcodeOutDir); err != nil {
		return 7
	}

	if err := removeSVGFiles(filespec.gcodeOutDir); err != nil {
		return 8
	}

	return 0
}

func processAndCombine(
	side string,
	inputGerberFiles []string,
	gcodeOutDir string,
	cfg gerber.PCB2GCodeConfig,
) error {
	logger := log.With().Str("side", side).Logger()

	if len(inputGerberFiles) == 0 {
		logger.Info().Str("side", side).Msg("no files for this side, skipping")
		return nil
	}

	// Process all front isolation layers.
	toCombine := []string{}
	for _, fname := range inputGerberFiles {
		outname, err := gcodetool.SpindleRampifyFile(fname)
		if err != nil {
			logger.Error().
				Str("file", fname).
				Err(err).Msg("error processing gerber file for isolation")
			continue
		}
		toCombine = append(toCombine, outname)
	}

	// Combine all GCode files into one program.
	outCode, err := gcodetool.Combine(toCombine...)
	if err != nil {
		logger.Error().Err(err).Msg("error combining layers")
		return err
	}

	// Prepend some extra info about this file.
	outCode.Lines = append(gerber.MillingArgs(cfg), outCode.Lines...)

	// Write out the combined file.
	combinedPath := filepath.Join(gcodeOutDir, "combined-"+side+gcodetool.GCodeFileExt)
	logger.Info().Str("file", combinedPath).Msg("combining output layers")
	if err := gcodetool.WriteFile(combinedPath, outCode); err != nil {
		logger.Error().Err(err).Msg("error writing combined layers")
		return err
	}

	// Log the final PCB size (according to this layer).
	size := gcodetool.Size(outCode.Lines)
	logger.Info().
		Str("x", fmt.Sprintf("%.3f..%.3f = %.3f", size.Xmin, size.Xmax, size.Width())).
		Str("y", fmt.Sprintf("%.3f..%.3f = %.3f", size.Ymin, size.Ymax, size.Height())).
		Str("z", fmt.Sprintf("%.3f..%.3f = %.3f", size.Zmin, size.Zmax, size.Elevation())).
		Msg("isolation size in mm")

	return nil
}

func processDrill(gcodeFiles *gerber.Files, gcodeOutDir string) error {
	if len(gcodeFiles.Drill) == 0 {
		log.Info().Msg("no drill files found, skipping drill processing")
		return nil
	}

	// Process drill files.
	toCombine := []string{}
	for _, fname := range gcodeFiles.Drill {
		outname, err := gcodetool.SpindleRampifyFile(fname)
		if err != nil {
			log.Error().Str("file", fname).Err(err).Msg("error processing gerber file for drilling")
			continue
		}
		toCombine = append(toCombine, outname)
	}

	// Combine all GCode files into one program.
	outFile, err := gcodetool.Combine(toCombine...)
	if err != nil {
		log.Error().Err(err).Msg("error combining layers")
		return err
	}

	// Write out the combined file.
	combinedPath := filepath.Join(gcodeOutDir, "combined-drill"+gcodetool.GCodeFileExt)
	log.Info().Str("file", combinedPath).Msg("combining output layers")
	if err := gcodetool.WriteFile(combinedPath, outFile); err != nil {
		log.Error().Err(err).Msg("error writing combined layers")
		return err
	}

	return nil
}

func processOutline(gcodeFiles *gerber.Files, gcodeOutDir string) error {
	if gcodeFiles.Outline == "" {
		log.Info().Msg("no outline file found, skipping processing")
		return nil
	}

	outname, err := gcodetool.SpindleRampifyFile(gcodeFiles.Outline)
	if err != nil {
		log.Error().Str("file", gcodeFiles.Outline).Err(err).Msg("error processing gerber file for outline")
		return err
	}

	log.Info().Str("file", outname).Msg("outline file processed")

	return nil
}

func removeSVGFiles(gcodeOutDir string) error {
	log.Info().Msg("removing SVG files")
	svgFiles, err := filepath.Glob(filepath.Join(gcodeOutDir, "*.svg"))
	if err != nil {
		log.Error().Err(err).Msg("could not find SVG files")
		return err
	}
	for _, fname := range svgFiles {
		err := os.Remove(fname)
		log.Debug().Str("fname", fname).Msg("removing")
		if err != nil {
			log.Error().Err(err).Msg("could not remove SVG file")
			return err
		}
	}
	return nil
}

// inOutSpec determines where we read from & write to.
// Errors are not returned, but are fatal to the process.
func inOutSpec(keepExtractedZip bool) InOutSpec {
	arg := os.Args[1]

	stat, err := os.Stat(arg)
	switch {
	case errors.Is(err, os.ErrNotExist):
		log.Fatal().Str("input", arg).Msg("input file/directory does not exist")
	case err != nil:
		log.Fatal().Str("input", arg).Err(err).Msg("input file/directory cannot be accessed")

	case stat.IsDir():
		var outdir string
		base := filepath.Base(arg)
		if strings.Contains(base, "gerber") {
			outdir = filepath.Join(filepath.Dir(arg), strings.ReplaceAll(base, "gerber", "gcode"))
		} else {
			outdir = filepath.Join(arg, "gcode")
		}
		return InOutSpec{
			gerberInDir: arg,
			gcodeOutDir: outdir,
		}

	case strings.ToLower(filepath.Ext(arg)) == ".zip":
		zipStem := stem(arg)
		tempZip, err := tempzip.Open(arg, zipStem+"-gerber", keepExtractedZip)
		if err != nil {
			log.Fatal().Str("input", arg).Err(err).Msg("error extracting ZIP file")
		}

		outdir := filepath.Join(filepath.Dir(arg), zipStem+"-gcode")
		return InOutSpec{
			gerberInDir: tempZip.Path(),
			gcodeOutDir: outdir,
			tempZipper:  &tempZip,
		}
	}

	log.Fatal().Str("input", arg).Msg("cannot handle input file, must be directory or ZIP file")
	return InOutSpec{}
}

func (s *InOutSpec) Close() {
	if s.tempZipper == nil {
		return
	}

	err := s.tempZipper.Close()
	if err != nil {
		log.Warn().Err(err).Msg("removing temporary directory for extracting your ZIP file")
	}
	s.tempZipper = nil
}

func stem(path string) string {
	base := filepath.Base(path)
	ext := filepath.Ext(base)
	return base[:len(base)-len(ext)]
}

func loadConfig() (gerber.PCB2GCodeConfig, error) {
	exe, err := os.Executable()
	if err != nil {
		return gerber.PCB2GCodeConfig{}, err
	}
	exeDir := filepath.Dir(exe)
	cfgPath := filepath.Join(exeDir, configFilename)

	paths := []string{
		configFilename,
		cfgPath,
	}

	for _, path := range paths {
		cfg, err := gerber.LoadConfig(path)
		switch {
		case errors.Is(err, os.ErrNotExist):
			if abspath, err := filepath.Abs(path); err == nil {
				path = abspath
			}
			log.Warn().Str("path", path).Msg("tried loading file, does not exist")
			// Ignore and try the next one.
			continue
		case err != nil:
			// Other errors are kinda fatal.
			return cfg, fmt.Errorf("loading %s: %w", path, err)
		default:
			// Loaded config just fine.
			return cfg, nil
		}
	}

	return gerber.PCB2GCodeConfig{}, os.ErrNotExist
}

func main() {
	os.Exit(cmd.MainRunner(Main))
}
