package main

import (
	"fmt"
	"os"

	"github.com/rs/zerolog/log"
	"gitlab.com/dr.sybren/gcodetool"
	"gitlab.com/dr.sybren/gcodetool/cmd"
)

func Main() int {
	if len(os.Args) < 2 {
		fmt.Println("Gimme a filename!")
		return 1
	}

	filename := os.Args[1]
	outpath, err := gcodetool.SpindleRampifyFile(filename)
	if err != nil {
		log.Fatal().Err(err).Msg("error inserting spindle ramps")
	}
	log.Info().Str("file", outpath).Msg("written output")
	return 0
}

func main() {
	os.Exit(cmd.MainRunner(Main))
}
