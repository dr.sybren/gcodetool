package gerber

import (
	"os"
	"strconv"
	"strings"

	"gopkg.in/yaml.v2"
)

type RPM int
type MM float64
type MMList []MM
type MMPerMinute int
type Percentage int

func (mm MM) String() string {
	return strconv.FormatFloat(float64(mm), 'f', 3, 64)
}

func (feed MMPerMinute) String() string {
	return strconv.Itoa(int(feed))
}

func (rpm RPM) String() string {
	return strconv.Itoa(int(rpm))
}

func (p Percentage) String() string {
	return strconv.Itoa(int(p)) + "%"
}

func (list MMList) String() string {
	items := []string{}
	for _, size := range list {
		items = append(items, size.String())
	}
	return strings.Join(items, ",")
}

type PCB2GCodeConfig struct {
	Basic   BasicArgs   `yaml:"basic"`
	Mill    MillArgs    `yaml:"mill"`
	Drill   DrillArgs   `yaml:"dril"`
	Outline OutlineArgs `yaml:"outline"`
	Meta    MetaArgs    `yaml:"meta"`
}
type MetaArgs struct {
	KeepExtractedZip bool `yaml:"keepExtractedZip"`
}
type BasicArgs struct {
	ZSafe                    MM  `yaml:"zsafe"`
	ZToolChangeWorkCoords    MM  `yaml:"zchangeWork"`
	ZToolChangeMachineCoords MM  `yaml:"zchangeMachine"`
	SpindleSpeed             RPM `yaml:"spindleSpeed"`
}
type MillArgs struct {
	HFeed                      MMPerMinute `yaml:"hFeed"`
	ZWork                      MM          `yaml:"zwork"`
	Overlap                    Percentage  `yaml:"overlap"`
	IsolationWidth             MM          `yaml:"isolationWidth"`
	IsolationMillDiameters     MMList      `yaml:"isolationMillDiameters"`
	CopperRemovalMillDiameters MMList      `yaml:"copperRemovalMillDiameters"`
}
type DrillArgs struct {
	ZDrill          MM            `yaml:"zdrill"`
	VFeed           MMPerMinute   `yaml:"vFeed"`
	DrillsAvailable []DrillParams `yaml:"drills"`
}
type DrillParams struct {
	Diameter MM          `yaml:"diameter"`
	VFeed    MMPerMinute `yaml:"vFeed,omitempty"`
	ZDrill   MM          `yaml:"zdrill,omitempty"`
}

// Diameters returns the drill parameters as comma-separated string.
func (da DrillArgs) Diameters() string {
	diameters := make(MMList, len(da.DrillsAvailable))
	for idx, drill := range da.DrillsAvailable {
		diameters[idx] = drill.Diameter
	}
	return diameters.String()
}

type OutlineArgs struct {
	CutterDiameter MM          `yaml:"cutterDiameter"`
	ZCut           MM          `yaml:"zcut"`
	HFeed          MMPerMinute `yaml:"hFeed"`
	VFeed          MMPerMinute `yaml:"vFeed"`
	InFeed         MM          `yaml:"inFeed"`
	BridgesWidth   MM          `yaml:"bridgesWidth"`
	BridgesNum     int         `yaml:"bridgesNum"`
	ZBridges       MM          `yaml:"zBridges"`
}

var defaultConfig = PCB2GCodeConfig{
	Basic: BasicArgs{
		ZSafe:                    1.0,
		ZToolChangeWorkCoords:    22,
		ZToolChangeMachineCoords: 0,
		SpindleSpeed:             20000,
	},
	Mill: MillArgs{
		HFeed:                      70,
		ZWork:                      -0.065,
		Overlap:                    25,
		IsolationWidth:             1000,
		IsolationMillDiameters:     MMList{0.25},
		CopperRemovalMillDiameters: MMList{0.2},
	},
	Drill: DrillArgs{
		ZDrill: -2.5,
		VFeed:  100,
		DrillsAvailable: []DrillParams{
			{Diameter: 0.6},
			{Diameter: 0.8},
			{Diameter: 0.9},
			{Diameter: 3.175, VFeed: 35},
		},
	},
	Outline: OutlineArgs{
		CutterDiameter: 2.0,
		ZCut:           -2.0,
		HFeed:          170,
		VFeed:          100,
		InFeed:         1.0,
		BridgesWidth:   2.0,
		BridgesNum:     2,
		ZBridges:       -0.75,
	},
}

func LoadConfig(yamlPath string) (PCB2GCodeConfig, error) {
	config := defaultConfig

	yamlBytes, err := os.ReadFile(yamlPath)
	if err != nil {
		return config, err
	}

	yaml.Unmarshal(yamlBytes, &config)

	return config, nil
}

func WriteDefaultConfig(yamlPath string) (PCB2GCodeConfig, error) {
	config := defaultConfig

	yamlBytes, err := yaml.Marshal(&config)
	if err != nil {
		return config, err
	}

	return config, os.WriteFile(yamlPath, yamlBytes, os.ModePerm)
}

func (c PCB2GCodeConfig) basicArgs() []string {
	return []string{
		"--metric",
		"--metricoutput",
		"--tolerance", "0.01",
		"--nog64",
		"--nom6",
		"--zsafe", c.Basic.ZSafe.String(),
		"--spinup-time", "0.5",
		"--path-finding-limit", "1000",
	}
}

// This whole function exists as a workaround for a Candle bug with heightmaps.
// More info at https://github.com/Denvi/Candle/issues/558
func (c PCB2GCodeConfig) zChangeWorkCoordsArgs() []string {
	return []string{
		"--zchange", c.Basic.ZToolChangeWorkCoords.String(),
	}
}

func (c PCB2GCodeConfig) zChangeMachineCoordsArgs() []string {
	return []string{
		"--zchange", c.Basic.ZToolChangeMachineCoords.String(), "--zchange-absolute",
	}
}

func (c PCB2GCodeConfig) millingArgs() []string {
	return []string{
		"--mill-feed", c.Mill.HFeed.String(),
		"--mill-speed", c.Basic.SpindleSpeed.String(),
		"--zwork", c.Mill.ZWork.String(),
		"--milling-overlap", c.Mill.Overlap.String(),
	}
}

func (c PCB2GCodeConfig) isolationArgs() []string {
	return []string{
		"--isolation-width", c.Mill.IsolationWidth.String(),
		"--mill-diameters", c.Mill.IsolationMillDiameters.String(),
		// "--voronoi",
	}
}

func (c PCB2GCodeConfig) copperRemovalArgs() []string {
	return []string{
		"--invert-gerbers",
		"--isolation-width", "1000",
		"--mill-diameters", c.Mill.CopperRemovalMillDiameters.String(),
	}
}

func (c PCB2GCodeConfig) drillArgs() []string {
	return []string{
		"--zdrill", c.Drill.ZDrill.String(),
		"--drill-speed", c.Basic.SpindleSpeed.String(),
		"--drill-feed", c.Drill.VFeed.String(),
		"--drill-side", "front",
		"--drills-available", c.Drill.Diameters(),
		"--nog81",
	}
}

func (c PCB2GCodeConfig) outlineArgs() []string {
	return []string{
		"--cutter-diameter", c.Outline.CutterDiameter.String(),
		"--zcut", c.Outline.ZCut.String(),
		"--cut-feed", c.Outline.HFeed.String(),
		"--cut-vertfeed", c.Outline.VFeed.String(),
		"--cut-speed", c.Basic.SpindleSpeed.String(),
		"--cut-infeed", c.Outline.InFeed.String(),
		"--bridges", c.Outline.BridgesWidth.String(),
		"--bridgesnum", strconv.Itoa(c.Outline.BridgesNum),
		"--zbridges", c.Outline.ZBridges.String(),
	}
}
