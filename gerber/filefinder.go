package gerber

import (
	"crypto/sha256"
	"encoding/hex"
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"github.com/rs/zerolog/log"
	"gitlab.com/dr.sybren/gcodetool"
)

type Files struct {
	// Front isolation pass.
	Front            string
	FrontCopperHoles string

	// Back isolation pass.
	Back            string
	BackCopperHoles string

	Drill   []string
	Outline string
}

func (f Files) AllFrontIsolation() []string {
	all := []string{}
	if f.Front != "" {
		all = append(all, f.Front)
	}
	if f.FrontCopperHoles != "" {
		all = append(all, f.FrontCopperHoles)
	}
	return all
}

func (f Files) AllBackIsolation() []string {
	all := []string{}
	if f.Back != "" {
		all = append(all, f.Back)
	}
	if f.BackCopperHoles != "" {
		all = append(all, f.BackCopperHoles)
	}
	return all
}

func FindFiles(gerberDir string) (*Files, error) {
	// More are documented at https://www.candorind.com/gerber-file-extensions/
	front, err := find(gerberDir, "*.GTL")
	if err != nil {
		return nil, err
	}
	frontCopperHoles, err := find(gerberDir, "*.GTA")
	if err != nil {
		return nil, err
	}
	back, err := find(gerberDir, "*.GBL")
	if err != nil {
		return nil, err
	}
	backCopperHoles, err := find(gerberDir, "*.GBA")
	if err != nil {
		return nil, err
	}
	drill, err := findAll(gerberDir, "*.DRL")
	if err != nil {
		return nil, err
	}
	drill, err = cleanupDrillFiles(drill)
	if err != nil {
		return nil, err
	}

	outline, err := find(gerberDir, "*.GKO")
	if err != nil {
		return nil, err
	}

	files := Files{
		Front:            front,
		FrontCopperHoles: frontCopperHoles,
		Back:             back,
		BackCopperHoles:  backCopperHoles,
		Drill:            drill,
		Outline:          outline,
	}
	return &files, nil
}

func find(gerberDir, glob string) (string, error) {
	found, err := findAll(gerberDir, glob)
	switch {
	case errors.Is(err, os.ErrNotExist):
		return "", nil
	case err != nil:
		return "", err
	case len(found) == 0:
		return "", nil
	}
	return found[0], nil
}

func findAll(gerberDir, glob string) ([]string, error) {
	globInDir := filepath.Join(gerberDir, glob)
	found, err := filepath.Glob(globInDir)
	if err != nil {
		return []string{}, fmt.Errorf("finding files in %s: %w", globInDir, err)
	}
	if len(found) == 0 {
		return []string{}, nil
	}
	return found, nil
}

// cleanupDrillFiles removes duplicates from drill files.
//
// EasyEDA exports the exact same data to Drill_NPTH_Through.DRL and
// Drill_PTH_Through.DRL, and we only need to process one of those.
func cleanupDrillFiles(drillFiles []string) ([]string, error) {
	seenSums := map[string]bool{}
	keepDrillFiles := []string{}

	log.Info().Strs("drillFiles", drillFiles).Msg("filtering out drill files")

	for _, fname := range drillFiles {
		// Read the file's contents
		contents, err := os.ReadFile(fname)
		if err != nil {
			return nil, fmt.Errorf("reading %s: %w", fname, err)
		}

		// Compute the SHA256 sum of the file contents, excluding comments. The
		// comments contain the filename, and thus each file would hash differently
		// even when the actual payload is identical.
		hasher := sha256.New()
		lines := strings.Split(string(contents), "\n")
		for _, line := range lines {
			if strings.HasPrefix(line, ";") {
				continue
			}
			hasher.Write([]byte(line))
		}
		fileHash := hex.EncodeToString(hasher.Sum(nil))

		// Skip if this hash has already been seen.
		if seenSums[fileHash] {
			log.Info().Str("fileHash", fileHash).Str("filename", fname).Msg("skipping drill file, already seen")
			continue
		}

		seenSums[fileHash] = true
		keepDrillFiles = append(keepDrillFiles, fname)
		log.Info().Str("fileHash", fileHash).Str("filename", fname).Msg("keeping drill file")
	}

	return keepDrillFiles, nil
}

func GCodeFilename(gerberFilePath string) string {
	base := filepath.Base(gerberFilePath)
	ext := filepath.Ext(base)
	return base[0:len(base)-len(ext)] + gcodetool.GCodeFileExt
}
