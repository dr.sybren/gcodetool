package gerber

import (
	"fmt"
	"testing"

	"github.com/256dpi/gcode"
	"github.com/rs/zerolog/log"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestOverrideDrillParams(t *testing.T) {
	config := DrillArgs{
		ZDrill: -2.5,
		VFeed:  100,
		DrillsAvailable: []DrillParams{
			{Diameter: 0.6},
			{Diameter: 0.8},
			{Diameter: 0.9},
			{Diameter: 1.2, VFeed: 70},
			{Diameter: 3.175, VFeed: 35, ZDrill: -13},
		},
	}

	testFile := gcode.File{
		Lines: []gcode.Line{
			parse("G94 (Millimeters per minute feed rate.)"),
			parse("G21 (Units == Millimeters.)"),
			parse("G53 G0 Z0 (Retract)"),
			parse("T1"),
			parse("M5 (Spindle stop.)"),
			parse("G4 P0.5"),
			parse("(MSG, Change tool bit to drill size 3.175mm)"),
			parse("M0 (Temporary machine stop.)"),
			parse("S20000"),
			parse("M3"),
			parse("G0 Z2"),
			parse("G1 F70"), // -> F35
			parse("G0 X-18.415 Y6.35"),
			parse("G1 Z-2"), // -> Z-13
			parse("G1 Z2"),
			parse("M5 (Spindle stop.)"),
			parse("G4 P0.5"),
			parse("(MSG, Change tool bit to drill size 1.2mm)"),
			parse("M0 (Temporary machine stop.)"),
			parse("M3"),
			parse("G0 Z2"),
			parse("G1 F22"), // -> F70
			parse("G0 X1.64 Y-1.06"),
			parse("G1 Z-100"), // -> Z-2.5
			parse("G1 Z2"),
			parse("M5 (Spindle stop.)"),
			parse("G4 P0.5"),
			parse("(MSG, Change tool bit to drill size 0.8mm)"),
			parse("M0 (Temporary machine stop.)"),
			parse("M3"),
			parse("G0 Z2"),
			parse("G1 F700"), // -> F100
			parse("G0 X16.14 Y-1.06"),
			parse("G1 Z-100"), // -> Z-2.5
			parse("G1 Z2"),
		},
	}

	expected := gcode.File{
		Lines: []gcode.Line{
			parse("G94 (Millimeters per minute feed rate.)"),
			parse("G21 (Units == Millimeters.)"),
			parse("G53 G0 Z0 (Retract)"),
			parse("T1"),
			parse("M5 (Spindle stop.)"),
			parse("G4 P0.5"),
			parse("(MSG, Change tool bit to drill size 3.175mm)"),
			parse("M0 (Temporary machine stop.)"),
			parse("S20000"),
			parse("M3"),
			parse("G0 Z2"),
			parse("G1 F35"),
			parse("G0 X-18.415 Y6.35"),
			parse("G1 Z-13"),
			parse("G1 Z2"),
			parse("M5 (Spindle stop.)"),
			parse("G4 P0.5"),
			parse("(MSG, Change tool bit to drill size 1.2mm)"),
			parse("M0 (Temporary machine stop.)"),
			parse("M3"),
			parse("G0 Z2"),
			parse("G1 F70"),
			parse("G0 X1.64 Y-1.06"),
			parse("G1 Z-2.5"),
			parse("G1 Z2"),
			parse("M5 (Spindle stop.)"),
			parse("G4 P0.5"),
			parse("(MSG, Change tool bit to drill size 0.8mm)"),
			parse("M0 (Temporary machine stop.)"),
			parse("M3"),
			parse("G0 Z2"),
			parse("G1 F100"),
			parse("G0 X16.14 Y-1.06"),
			parse("G1 Z-2.5"),
			parse("G1 Z2"),
		},
	}

	logger := log.Logger
	outfile := overrideDrillParams(logger, &testFile, config)
	require.Equal(t, len(expected.Lines), len(outfile.Lines))
	assert.Equal(t, expected.Lines, outfile.Lines)
}

func parse(line string) gcode.Line {
	gcodeLine, err := gcode.ParseLine(line)
	if err != nil {
		panic(fmt.Errorf("unable to parse as GCode: %q", line))
	}
	return gcodeLine
}
