package gerber

import (
	"fmt"
	"math"
	"path/filepath"
	"regexp"
	"strconv"

	"github.com/256dpi/gcode"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.com/dr.sybren/gcodetool"
)

var drillBitSizeMsgRe = regexp.MustCompile("MSG, Change tool bit to drill size ([0-9.]+)mm")

func OverrideDrillParams(inpath, outpath string, config DrillArgs) error {
	gcodefile, err := gcodetool.ParseFile(inpath)
	if err != nil {
		return fmt.Errorf("unable to parse %s: %w", inpath, err)
	}

	logger := log.With().Str("path", filepath.Base(inpath)).Logger()
	processedFile := overrideDrillParams(logger, gcodefile, config)

	err = gcodetool.WriteFile(outpath, processedFile)
	if err != nil {
		return fmt.Errorf("unable to write %s: %w", outpath, err)
	}

	return nil
}

func overrideDrillParams(logger zerolog.Logger, infile *gcode.File, config DrillArgs) *gcode.File {
	outfile := gcode.File{
		Lines: make([]gcode.Line, 0, len(infile.Lines)),
	}

	drill := config.defaultParams()
	for _, line := range infile.Lines {
		// Figure out which drill to use.
		usedDrillSize := findDrillBitSize(logger, line)
		if usedDrillSize != "" {
			diameter, err := strconv.ParseFloat(usedDrillSize, 64)
			if err != nil {
				panic(fmt.Errorf("error parsing %q as float: %w", usedDrillSize, err))
			}

			drill = config.forDiameter(MM(diameter))
			logger.Info().Interface("drill", drill).Msg("switching drill")
		}

		// Copy the line so we can modify it without altering the original.
		outLine := gcode.Line{
			Codes:   make([]gcode.GCode, len(line.Codes)),
			Comment: line.Comment,
		}
		copy(outLine.Codes, line.Codes)

		// Replace vertical feed rate.
		if len(outLine.Codes) >= 2 {
			// G1 F...
			if outLine.Codes[0].Letter == "G" && outLine.Codes[0].Value == 1 &&
				outLine.Codes[1].Letter == "F" {
				outLine.Codes[1].Value = float64(drill.VFeed)
			}
		}

		// Replace drill depth
		if len(outLine.Codes) >= 2 {
			// G1 Z...
			if outLine.Codes[0].Letter == "G" && outLine.Codes[0].Value == 1 &&
				outLine.Codes[1].Letter == "Z" && outLine.Codes[1].Value < 0 {
				outLine.Codes[1].Value = float64(drill.ZDrill)
			}
		}

		outfile.Lines = append(outfile.Lines, outLine)
	}

	return &outfile
}

func findDrillBitSize(logger zerolog.Logger, line gcode.Line) string {
	matches := drillBitSizeMsgRe.FindStringSubmatch(line.Comment)
	if len(matches) > 0 {
		logger.Debug().Strs("matches", matches).Msg("found matching line")
		return matches[1]
	}

	for idx := range line.Codes {
		matches := drillBitSizeMsgRe.FindStringSubmatch(line.Codes[idx].Comment)
		if len(matches) > 0 {
			logger.Debug().Strs("matches", matches).Msg("found matching code")
			return matches[1]
		}
	}

	return ""
}

func (da DrillArgs) defaultParams() DrillParams {
	return DrillParams{
		VFeed:  da.VFeed,
		ZDrill: da.ZDrill,
	}
}

func (da DrillArgs) forDiameter(diameter MM) DrillParams {
	params := da.defaultParams()
	drill := da.closest(diameter)
	if drill.VFeed != 0 {
		params.VFeed = drill.VFeed
	}
	if drill.ZDrill != 0 {
		params.ZDrill = drill.ZDrill
	}
	params.Diameter = diameter
	return params
}

func (da DrillArgs) closest(diameter MM) DrillParams {
	bestDiff := math.Inf(1)
	var bestDrill DrillParams

	for _, drill := range da.DrillsAvailable {
		diff := math.Abs(float64(drill.Diameter - diameter))
		if diff < bestDiff {
			bestDiff = diff
			bestDrill = drill
		}
	}

	return bestDrill
}
