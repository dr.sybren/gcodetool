package gerber

import (
	"context"
	"errors"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"time"

	"github.com/256dpi/gcode"
	"github.com/mattn/go-colorable"
	"github.com/rs/zerolog/log"
	"gitlab.com/dr.sybren/gcodetool"
)

type PCB2GCodeRunner struct {
	exeName     string
	outDir      string
	gerberFiles *Files
	outFiles    *Files
	config      PCB2GCodeConfig
}

func PCB2GCode(outDir string, gerberFiles *Files, config PCB2GCodeConfig) (*Files, error) {
	// The whole lot shouldn't take more than a minute.
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Minute)
	defer cancel()

	exePath, err := exec.LookPath("pcb2gcode")
	if err != nil {
		return nil, err
	}

	runner := PCB2GCodeRunner{
		exeName:     exePath,
		outDir:      outDir,
		gerberFiles: gerberFiles,
		outFiles:    &Files{},
		config:      config,
	}

	if err := runner.frontCopperRemoval(ctx); err != nil {
		return nil, err
	}
	if err := runner.front(ctx); err != nil {
		return nil, err
	}

	if err := runner.backCopperRemoval(ctx); err != nil {
		return nil, err
	}
	if err := runner.back(ctx); err != nil {
		return nil, err
	}

	for _, gerberFile := range gerberFiles.Drill {
		out, err := runner.drill(ctx, gerberFile)
		if err != nil {
			return nil, err
		}
		if out == "" {
			continue
		}

		processed := filepath.Join(filepath.Dir(out), "processed-"+filepath.Base(out))
		if err := OverrideDrillParams(out, processed, config.Drill); err != nil {
			return nil, err
		}

		runner.outFiles.Drill = append(runner.outFiles.Drill, processed)
	}
	return runner.outFiles, nil
}

// Normal run, for front layer and outline.
func (r *PCB2GCodeRunner) front(ctx context.Context) error {
	r.outFiles.Front = filepath.Join(r.outDir, GCodeFilename(r.gerberFiles.Front))
	r.outFiles.Outline = filepath.Join(r.outDir, "outline"+gcodetool.GCodeFileExt)

	log.Info().
		Str("input", r.gerberFiles.Front).
		Str("output", r.outFiles.Front).
		Msg("running pcb2gcode for front layer")

	millingArgs := r.config.millingArgs()
	isolationArgs := r.config.isolationArgs()
	outlineArgs := r.config.outlineArgs()
	zchangeArgs := r.config.zChangeWorkCoordsArgs()

	args := []string{
		"--front", r.gerberFiles.Front,
		"--front-output", filepath.Base(r.outFiles.Front),
		"--outline", r.gerberFiles.Outline,
		"--outline-output", filepath.Base(r.outFiles.Outline),
	}
	args = append(args, millingArgs...)
	args = append(args, isolationArgs...)
	args = append(args, outlineArgs...)
	args = append(args, zchangeArgs...)
	return r.run(ctx, args)
}

func (r *PCB2GCodeRunner) frontCopperRemoval(ctx context.Context) error {
	_, err := os.Stat(r.gerberFiles.FrontCopperHoles)
	switch {
	case errors.Is(err, os.ErrNotExist):
		return nil
	case err != nil:
		return err
	}

	r.outFiles.FrontCopperHoles = filepath.Join(r.outDir, GCodeFilename(r.gerberFiles.FrontCopperHoles))

	millingArgs := r.config.millingArgs()
	copperRemovalArgs := r.config.copperRemovalArgs()
	zchangeArgs := r.config.zChangeWorkCoordsArgs()

	log.Info().
		Str("input", r.gerberFiles.FrontCopperHoles).
		Str("output", r.outFiles.FrontCopperHoles).
		Msg("running pcb2gcode for copper removal")

	args := []string{
		"--front", r.gerberFiles.FrontCopperHoles,
		"--front-output", filepath.Base(r.outFiles.FrontCopperHoles),
	}
	args = append(args, millingArgs...)
	args = append(args, copperRemovalArgs...)
	args = append(args, zchangeArgs...)
	return r.run(ctx, args)
}

// Normal run, for back layer and outline.
func (r *PCB2GCodeRunner) back(ctx context.Context) error {
	_, err := os.Stat(r.gerberFiles.Back)
	switch {
	case errors.Is(err, os.ErrNotExist):
		return nil
	case err != nil:
		return err
	}

	r.outFiles.Back = filepath.Join(r.outDir, GCodeFilename(r.gerberFiles.Back))
	r.outFiles.Outline = filepath.Join(r.outDir, "outline"+gcodetool.GCodeFileExt)

	log.Info().
		Str("input", r.gerberFiles.Back).
		Str("output", r.outFiles.Back).
		Msg("running pcb2gcode for back layer")

	millingArgs := r.config.millingArgs()
	isolationArgs := r.config.isolationArgs()
	outlineArgs := r.config.outlineArgs()
	zchangeArgs := r.config.zChangeWorkCoordsArgs()

	args := []string{
		"--back", r.gerberFiles.Back,
		"--back-output", filepath.Base(r.outFiles.Back),
		"--outline", r.gerberFiles.Outline,
	}
	args = append(args, millingArgs...)
	args = append(args, isolationArgs...)
	args = append(args, outlineArgs...)
	args = append(args, zchangeArgs...)
	return r.run(ctx, args)
}

func (r *PCB2GCodeRunner) backCopperRemoval(ctx context.Context) error {
	_, err := os.Stat(r.gerberFiles.BackCopperHoles)
	switch {
	case errors.Is(err, os.ErrNotExist):
		return nil
	case err != nil:
		return err
	}

	r.outFiles.BackCopperHoles = filepath.Join(r.outDir, GCodeFilename(r.gerberFiles.BackCopperHoles))

	millingArgs := r.config.millingArgs()
	copperRemovalArgs := r.config.copperRemovalArgs()
	zchangeArgs := r.config.zChangeWorkCoordsArgs()

	log.Info().
		Str("input", r.gerberFiles.BackCopperHoles).
		Str("output", r.outFiles.BackCopperHoles).
		Msg("running pcb2gcode for copper removal")

	args := []string{
		"--back", r.gerberFiles.BackCopperHoles,
		"--back-output", filepath.Base(r.outFiles.BackCopperHoles),
	}
	args = append(args, millingArgs...)
	args = append(args, copperRemovalArgs...)
	args = append(args, zchangeArgs...)
	return r.run(ctx, args)
}

func (r *PCB2GCodeRunner) drill(ctx context.Context, gerberFile string) (string, error) {
	_, err := os.Stat(gerberFile)
	switch {
	case errors.Is(err, os.ErrNotExist):
		return "", nil
	case err != nil:
		return "", err
	}

	out := filepath.Join(r.outDir, GCodeFilename(gerberFile))

	log.Info().
		Str("input", gerberFile).
		Str("output", out).
		Msg("running pcb2gcode for drill holes")

	drillArgs := r.config.drillArgs()
	zchangeArgs := r.config.zChangeMachineCoordsArgs()

	args := []string{
		"--drill", gerberFile,
		"--drill-output", filepath.Base(out),
	}
	args = append(args, drillArgs...)
	args = append(args, zchangeArgs...)

	err = r.run(ctx, args)
	if err != nil {
		return "", err
	}

	// It could be that the drill file existed, but had no info in it.
	_, err = os.Stat(out)
	switch {
	case errors.Is(err, os.ErrNotExist):
		return "", nil
	case err != nil:
		return "", err
	}

	return out, nil
}

func (r *PCB2GCodeRunner) run(ctx context.Context, cliArgs []string) error {
	basicArgs := r.config.basicArgs()

	cliArgs = append(cliArgs, basicArgs...)
	cliArgs = append(cliArgs, "--output-dir", r.outDir)

	cliAsString := r.exeName + " " + strings.Join(cliArgs, " ")
	log.Info().Msgf("running: %s", cliAsString)

	cmd := exec.CommandContext(ctx, r.exeName, cliArgs...)
	out, err := cmd.CombinedOutput()

	// Print the output.
	lines := strings.Split(string(out), "\n")
	stdout := colorable.NewColorableStdout()

	for _, line := range lines {
		line = strings.TrimSpace(line)

		// Hide "Repairing" lines. They're spammy and I already know that EasyEDA is
		// making a mess.
		if strings.Contains(line, "Repairing invalid contour") ||
			(strings.Contains(line, "Importing ") && strings.Contains(line, "not specified.") ||
				line == "END." || line == "DONE.") {
			continue
		}

		// Highlight warnings, except when it's one that hasn't been an issue so far.
		ansi := ""
		ansiClose := "\033[0m"
		switch {
		case strings.Contains(line, "Error:"):
			ansi = "\033[91m"
		case strings.Contains(line, "Warning:") &&
			!strings.Contains(line, "Board dimensions unknown."):
			ansi = "\033[38;5;214m"
		case strings.Contains(line, "Info:"):
			ansi = "\033[95m"
		default:
			ansiClose = ""
		}

		maxLen := 250
		if len(line) > maxLen {
			line = line[:maxLen-3] + "..."
		}
		fmt.Fprintln(stdout, ansi+line+ansiClose)
	}

	if err != nil {
		return err
	}
	return nil
}

// MillingArgs returns the pcb2gcode CLI arguments used when milling, as GCode comments.
func MillingArgs(c PCB2GCodeConfig) []gcode.Line {
	millingArgs := c.millingArgs()
	isolationArgs := c.isolationArgs()
	copperRemovalArgs := c.copperRemovalArgs()

	return gcodetool.CommentLines(
		"All Milling: "+strings.Join(millingArgs, " "),
		"Isolation: "+strings.Join(isolationArgs, " "),
		"Copper Removal: "+strings.Join(copperRemovalArgs, " "),
	)
}
