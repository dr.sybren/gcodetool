package tempzip

import (
	"os"
	"path/filepath"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

const toolsGoContents = `//go:build tools

// This file will never be built, but 'go mod tidy' will see the packages
// imported here as dependencies and not remove them from 'go.mod'.

package main

import (
	_ "github.com/tc-hib/go-winres"
	_ "golang.org/x/vuln/cmd/govulncheck"
	_ "honnef.co/go/tools/cmd/staticcheck"
)
`

func TestOpenHappy(t *testing.T) {
	zipper, err := Open("test_files/mage.zip", "extracted")
	require.NoError(t, err)

	defer zipper.Close()
	assert.Equal(t, filepath.Join("test_files", "extracted"), zipper.dirFullPath)

	require.DirExists(t, "test_files/extracted")
	require.DirExists(t, "test_files/extracted/magefiles")
	assert.FileExists(t, "test_files/extracted/mage.go")
	assert.FileExists(t, "test_files/extracted/magefiles/build.go")
	assert.FileExists(t, "test_files/extracted/magefiles/clean.go")
	assert.FileExists(t, "test_files/extracted/magefiles/tools/tools.go")

	contents, err := os.ReadFile("test_files/extracted/magefiles/tools/tools.go")
	require.NoError(t, err)
	assert.Equal(t,
		strings.ReplaceAll(toolsGoContents, "\r\n", "\n"),
		strings.ReplaceAll(string(contents), "\r\n", "\n"),
	)
}

func TestCloseHappy(t *testing.T) {
	zipper, err := Open("test_files/mage.zip", "extracted")
	require.NoError(t, err)

	assert.NoError(t, zipper.Close())
	assert.NoDirExists(t, "test_files/extracted")
}

func TestOpenUnhappy(t *testing.T) {
	zipper, err := Open("test_files/nonexistent.zip", "extracted")
	assert.Error(t, err)
	assert.Equal(t, "", zipper.dirFullPath)
	assert.NoDirExists(t, "test_files/extracted")
}
