package tempzip

import (
	"archive/zip"
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"
)

type TempZipper struct {
	dirFullPath      string
	keepExtractedZip bool
}

// Open extracts a zipfile into "dir-of-zip/dirBaseName/".
func Open(zipfile, dirBaseName string, keepExtractedZip bool) (TempZipper, error) {
	dirFullPath := filepath.Join(filepath.Dir(zipfile), dirBaseName)

	if err := mkdirAndExtract(zipfile, dirFullPath); err != nil {
		return TempZipper{}, err
	}

	return TempZipper{dirFullPath, keepExtractedZip}, nil
}

// Path returns the path into which the ZIP file was extracted.
func (tz TempZipper) Path() string {
	return tz.dirFullPath
}

// Close removes the directory into which the ZIP file was extracted.
func (tz TempZipper) Close() error {
	if tz.keepExtractedZip {
		return nil
	}
	if tz.dirFullPath == "" {
		return errors.New("refusing to delete \"\"")
	}
	return os.RemoveAll(tz.dirFullPath)
}

func mkdirAndExtract(zipfile, directory string) error {
	if err := os.MkdirAll(directory, os.ModePerm); err != nil {
		return err
	}

	err := extractInto(zipfile, directory)
	if err != nil {
		os.RemoveAll(directory)
		return err
	}
	return nil
}

func extractInto(zipfile, directory string) error {
	// Open a zip archive for reading.
	reader, err := zip.OpenReader(zipfile)
	if err != nil {
		return err
	}
	defer reader.Close()

	// Iterate through the files in the archive,
	// printing some of their contents.
	for _, f := range reader.File {
		var err error

		switch {
		case f.FileInfo().IsDir():
			err = makeDir(f, directory)
		default:
			err = extractFile(f, directory)
		}

		if err != nil {
			return err
		}
	}
	return nil
}

func extractFile(file *zip.File, directory string) error {
	outname := filepath.Join(directory, file.Name)
	writer, err := os.Create(outname)
	if err != nil {
		return err // already contains the filename and reason.
	}
	defer writer.Close()

	reader, err := file.Open()
	if err != nil {
		return fmt.Errorf("opening %s: %w", file.Name, err)
	}
	defer reader.Close()

	_, err = io.Copy(writer, reader)
	if err != nil {
		return fmt.Errorf("extracting %s: %w", file.Name, err)
	}

	if err := writer.Close(); err != nil {
		return fmt.Errorf("closing %s: %w", outname, err)
	}

	if err := reader.Close(); err != nil {
		return fmt.Errorf("closing %s: %w", file.Name, err)
	}

	return nil
}

func makeDir(file *zip.File, directory string) error {
	outname := filepath.Join(directory, file.Name)
	return os.MkdirAll(outname, os.ModePerm)
}
